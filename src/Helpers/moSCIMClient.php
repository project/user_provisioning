<?php

namespace Drupal\user_provisioning\Helpers;

use Drupal\Core\Render\Markup;
use Drupal\user_provisioning\moUserProvisioningConstants;

/**
 *
 */
class moSCIMClient {
  private $url_path;
  private $base_url;

  /**
   * Constructor for the Provider Specific Provisioning tab.
   */
  public function __construct() {
    $base_url = \Drupal::request()->getSchemeAndHttpHost() . \Drupal::request()->getBaseUrl();
    $this->base_url = $base_url;
    $this->url_path = $base_url . '/' . \Drupal::service('extension.list.module')->getPath('user_provisioning') . '/images';
  }

  /**
   * @return array of providers for provider specific provisioning
   */
  public function providerList($custom = FALSE): array {
    $tab_url = $this->base_url . moUserProvisioningConstants::USER_PROVISIONING;

    if ($custom == 'custom') {
      return [
        Markup::create('<a class="use-ajax" href="' . $tab_url . '/configure_app/custom"><div><img class="mo_user_provisioning_img_logo" alt="Custom App" src="' . $this->url_path . '/customapp.png" ><br><strong>Custom Provider</strong></div></a>'),
      ];
    }
    else {
      return [
        Markup::create('<a class="use-ajax" href="' . $tab_url . '/configure_app/aws_sso"><div><img class="mo_user_provisioning_img_logo" alt="AWS SSO" src="' . $this->url_path . '/AWS.png" ><br><strong>AWS SSO</strong></div></a>'),
        Markup::create('<a class="use-ajax" href="' . $tab_url . '/configure_app/azure_ad"><div><img class="mo_user_provisioning_img_logo" alt="Azure AD" src="' . $this->url_path . '/azure.png" ><br><strong>Azure Active Directory</strong></div></a>'),
        Markup::create('<a class="use-ajax" href="' . $tab_url . '/configure_app/okta"><div><img class="mo_user_provisioning_img_logo" alt="Okta" src="' . $this->url_path . '/okta.png" ><br><strong>Okta</strong></div></a>'),
        Markup::create('<a class="use-ajax" href="' . $tab_url . '/configure_app/onelogin"><div><img class="mo_user_provisioning_img_logo" alt="OneLogin" src="' . $this->url_path . '/onelogin.png" ><br><strong>OneLogin</strong></div></a>'),
        Markup::create('<a class="use-ajax" href="' . $tab_url . '/configure_app/google"><div><img class="mo_user_provisioning_img_logo" alt="Google Apps" src="' . $this->url_path . '/google.png" ><br><strong>Google Apps</strong></div></a>'),
        Markup::create('<a class="use-ajax" href="' . $tab_url . '/configure_app/cyberark"><div><img class="mo_user_provisioning_img_logo" alt="CyberArk" src="' . $this->url_path . '/cyberark.png" ><br><strong>CyberArk</strong></div></a>'),
        Markup::create('<a class="use-ajax" href="' . $tab_url . '/configure_app/miniorange"><div><img class="mo_user_provisioning_img_logo" alt="miniOrange" src="' . $this->url_path . '/miniorange.png" ><br><strong>miniOrange</strong></div></a>'),
        Markup::create('<a class="use-ajax" href="' . $tab_url . '/configure_app/jumpcloud"><div><img class="mo_user_provisioning_img_logo" alt="JumpCloud" src="' . $this->url_path . '/jumpcloud.png" ><br><strong>JumpCloud</strong></div></a>'),
        Markup::create('<a class="use-ajax" href="' . $tab_url . '/configure_app/ping"><div><img class="mo_user_provisioning_img_logo" alt="PingOne" src="' . $this->url_path . '/pingone.png" ><br><strong>PingOne</strong></div></a>'),
        Markup::create('<a class="use-ajax" href="' . $tab_url . '/configure_app/centrify"><div><img class="mo_user_provisioning_img_logo" alt="Centrify" src="' . $this->url_path . '/centrify.png" ><br><strong>Centrify</strong></div></a>'),
        Markup::create('<a class="use-ajax" href="' . $tab_url . '/configure_app/salesforce"><div><img class="mo_user_provisioning_img_logo" alt="Salesforce" src="' . $this->url_path . '/salesforce.png" ><br><strong>Salesforce</strong></div></a>'),
        Markup::create('<a class="use-ajax" href="' . $tab_url . '/configure_app/oracle"><div><img class="mo_user_provisioning_img_logo" alt="Oracle" src="' . $this->url_path . '/oracle.png" ><br><strong>Oracle</strong></div></a>'),
        Markup::create('<a class="use-ajax" href="' . $tab_url . '/configure_app/drupal"><div><img class="mo_user_provisioning_img_logo" alt="Drupal" src="' . $this->url_path . '/Drupal.png" ><br><strong>Drupal</strong></div></a>'),
        Markup::create('<a class="use-ajax" href="' . $tab_url . '/configure_app/joomla"><div><img class="mo_user_provisioning_img_logo" alt="Joomla" src="' . $this->url_path . '/Joomla.png" ><br><strong>Joomla</strong></div></a>'),
        Markup::create('<a class="use-ajax" href="' . $tab_url . '/configure_app/wordpress"><div><img class="mo_user_provisioning_img_logo" alt="WordPress" src="' . $this->url_path . '/wordpress.png" ><br><strong>WordPress</strong></div></a>'),
        Markup::create('<a class="use-ajax" href="' . $tab_url . '/configure_app/alexis"><div><img class="mo_user_provisioning_img_logo" alt="AlexisHR" src="' . $this->url_path . '/alexis.png" ><br><strong>AlexisHR Provisioning</strong></div></a>'),
        Markup::create('<a class="use-ajax" href="' . $tab_url . '/configure_app/anaplan"><div><img class="mo_user_provisioning_img_logo" alt="Anaplan" src="' . $this->url_path . '/anaplan.png" ><br><strong>Anaplan SCIM API</strong></div></a>'),
        Markup::create('<a class="use-ajax" href="' . $tab_url . '/configure_app/blink"><div><img class="mo_user_provisioning_img_logo" alt="Blink" src="' . $this->url_path . '/blink.png" ><br><strong>Blink</strong></div></a>'),
        Markup::create('<a class="use-ajax" href="' . $tab_url . '/configure_app/calendly"><div><img class="mo_user_provisioning_img_logo" alt="Calendly" src="' . $this->url_path . '/calendly.png" ><br><strong>Calendly</strong></div></a>'),
        Markup::create('<a class="use-ajax" href="' . $tab_url . '/configure_app/circlehd"><div><img class="mo_user_provisioning_img_logo" alt="CircleHD" src="' . $this->url_path . '/circleHD.png" ><br><strong>CircleHD</strong></div></a>'),
        Markup::create('<a class="use-ajax" href="' . $tab_url . '/configure_app/curity"><div><img class="mo_user_provisioning_img_logo" alt="Curity" src="' . $this->url_path . '/curity.png" ><br><strong>Curity Identity Server</strong></div></a>'),
        Markup::create('<a class="use-ajax" href="' . $tab_url . '/configure_app/federated_dir"><div><img class="mo_user_provisioning_img_logo" alt="Federated Directory" src="' . $this->url_path . '/federated_directory.png" ><br><strong>Federated Directory</strong></div></a>'),
        Markup::create('<a class="use-ajax" href="' . $tab_url . '/configure_app/fusionauth"><div><img class="mo_user_provisioning_img_logo" alt="FusionAuth" src="' . $this->url_path . '/fusionauth.png" ><br><strong>FusionAuth</strong></div></a>'),
        Markup::create('<a class="use-ajax" href="' . $tab_url . '/configure_app/github"><div><img class="mo_user_provisioning_img_logo" alt="GitHub" src="' . $this->url_path . '/github.png" ><br><strong>GitHub Business</strong></div></a>'),
        Markup::create('<a class="use-ajax" href="' . $tab_url . '/configure_app/idaas"><div><img class="mo_user_provisioning_img_logo" alt="Idaas.nl" src="' . $this->url_path . '/idaas.png" ><br><strong>idaas.nl</strong></div></a>'),
        Markup::create('<a class="use-ajax" href="' . $tab_url . '/configure_app/monokee"><div><img class="mo_user_provisioning_img_logo" alt="Monokee" src="' . $this->url_path . '/monokee.png" ><br><strong>Monokee</strong></div></a>'),
        Markup::create('<a class="use-ajax" href="' . $tab_url . '/configure_app/netiq"><div><img class="mo_user_provisioning_img_logo" alt="NetIQ" src="' . $this->url_path . '/netiq.png" ><br><strong>NetIQ Identity Manager</strong></div></a>'),
        Markup::create('<a class="use-ajax" href="' . $tab_url . '/configure_app/workday"><div><img class="mo_user_provisioning_img_logo" alt="Workday Peakon" src="' . $this->url_path . '/workday.png" ><br><strong>Workday Peakon</strong></div></a>'),
        Markup::create('<a class="use-ajax" href="' . $tab_url . '/configure_app/reward"><div><img class="mo_user_provisioning_img_logo" alt="Reward Gateway" src="' . $this->url_path . '/rewardgateway.png" ><br><strong>Reward Gateway</strong></div></a>'),
        Markup::create('<a class="use-ajax" href="' . $tab_url . '/configure_app/sailpoint"><div><img class="mo_user_provisioning_img_logo" alt="Reward Gateway" src="' . $this->url_path . '/sailpoint.png" ><br><strong>SailPoint</strong></div></a>'),
        Markup::create('<a class="use-ajax" href="' . $tab_url . '/configure_app/sap"><div><img class="mo_user_provisioning_img_logo" alt="Reward Gateway" src="' . $this->url_path . '/sap.png" ><br><strong>SAP</strong></div></a>'),
        Markup::create('<a class="use-ajax" href="' . $tab_url . '/configure_app/trello"><div><img class="mo_user_provisioning_img_logo" alt="Trello" src="' . $this->url_path . '/trello.png" ><br><strong>Trello</strong></div></a>'),
        Markup::create('<a class="use-ajax" href="' . $tab_url . '/configure_app/wso2"><div><img class="mo_user_provisioning_img_logo" alt="AWS SSO" src="' . $this->url_path . '/wso2.png" ><br><strong>WSO2 Charon</strong></div></a>'),
      ];
    }
  }

}
