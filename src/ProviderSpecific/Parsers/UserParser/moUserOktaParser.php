<?php

namespace Drupal\user_provisioning\ProviderSpecific\Parsers\UserParser;

use Drupal\Core\Config\Config;
use Drupal\Core\Config\ImmutableConfig;
use Drupal\Core\Entity\EntityInterface;
use Drupal\user_provisioning\Helpers\moProviderSpecificProvisioning;
use Drupal\user_provisioning\Helpers\moUserProvisioningLogger;
use Drupal\user_provisioning\ProviderSpecific\Parsers\moResourceParserInterface;

/**
 *
 */
class moUserOktaParser implements moResourceParserInterface {

  private Config $config_factory;
  private ImmutableConfig $config;
  private moUserProvisioningLogger $mo_logger;

  /**
   *
   */
  public function __construct() {
    $this->mo_logger = new moUserProvisioningLogger();
    $this->config_factory = \Drupal::configFactory()->getEditable('okta_user_sync.settings');
    $this->config = \Drupal::config('user_provisioning.settings');
  }

  /**
   *
   */
  public function search($resource_id): string {

    $this->mo_logger->addLog("Creating search request parameter.", __LINE__, __FUNCTION__, __FILE__);
    return 'users?search=profile.email eq "' . $resource_id . '"';
  }

  /**
   *
   */
  public function get($resource_id) {
    // @todo Implement get() method.
  }

  /**
   *
   */
  public function post(EntityInterface $entity) {
    $enable_auto_provisioning_okta = $this->config_factory->get('okta_user_sync_create_user_auto_radio_button');
    $enable_manual_provisioning_okta = $this->config_factory->get('okta_user_sync_create_user_manual_radio_button');
    $isManualSyncRequest = $this->config_factory->get('okta_user_sync_request_to_manual_sync');
    // The bare minimum attributes.
    $post_request_body = '';

    $username_attr = $this->config_factory->get('okta_user_sync_basic_mail_mapping');
    $email_attr = $this->config_factory->get('okta_user_sync_basic_mail_mapping');

    $user_email = $entity->getEmail();
    $username = $entity->getEmail();

    if (isset($email_attr)) {
      if ($email_attr == 'uid') {
        $user_email = $entity->id();
      }
      else {
        $user_email = $entity->get($email_attr)->value;
      }
    }

    if (isset($username_attr)) {
      if ($username_attr == 'uid') {
        $username = $entity->id();
      }
      else {
        $username = $entity->get($username_attr)->value;
      }
    }

    if ($enable_auto_provisioning_okta == '0' ||($enable_manual_provisioning_okta == '0' && $isManualSyncRequest == TRUE)) {
      $post_request_body = [
        'profile' => [
          'login' => $username,
          'email' => $user_email,
          'firstName' => $entity->getDisplayName(),
          'lastName' => $entity->getDisplayName(),
        ],
      ];
    }
    elseif ($enable_auto_provisioning_okta == '1') {

      $user_password = $this->getUserPassword($entity);
      $post_request_body = [
        'profile' => [
          'login' => $username,
          'email' => $user_email,
          'firstName' => $entity->getDisplayName(),
          'lastName' => $entity->getDisplayName(),
        ],
        'credentials' => [
          'password' => ['value' => $user_password],
        ],

      ];
    }
    $this->config_factory->set('okta_user_sync_request_to_manual_sync', FALSE)->save();

    return $post_request_body;

  }

  /**
   *
   */
  private function getUserPassword(EntityInterface $entity) {
    $name = $entity->getAccountName();
    $enable_auto_pass = \Drupal::configFactory()->getEditable('okta_user_sync.settings')->get('mo_okta_' . $name . '_dependency');
    $prov_specific = new moProviderSpecificProvisioning();
    return $prov_specific->decrypt_data($enable_auto_pass, $name);
  }

  /**
   *
   */
  public function put(array $resource) {
    // @todo Implement put() method.
  }

  /**
   *
   */
  public function patch(array $resource) {
    // @todo Implement patch() method.
  }

  /**
   *
   */
  public function delete(array $resource) {
    // @todo Implement delete() method.
  }

  /**
   *
   */
  public function deactivate(EntityInterface $entity) {
    return $entity->getEmail();

  }

}
