<?php

namespace Drupal\user_provisioning\Form;

use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\RedirectCommand;
use Drupal\Core\Ajax\ReplaceCommand;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;

/**
 *
 */
class MoUserProvisioningRemoveAccount extends FormBase {

  /**
   *
   */
  public function getFormId() {
    return 'miniorange_user_provisioning_remove_account';
  }

  /**
   *
   */
  public function buildForm(array $form, FormStateInterface $form_state, $options = NULL) {
    $form['mo_user_provision_add_css'] = [
      '#attached' => [
        'library' => [
          'user_provisioning/user_provisioning.admin',
        ],
      ],
    ];

    $form['#prefix'] = '<div id="modal_example_form">';
    $form['#suffix'] = '</div>';
    $form['status_messages'] = [
      '#type' => 'status_messages',
      '#weight' => -10,
    ];

    $form['mo_user_provisioning_content'] = [
      '#markup' => t('Are you sure you want to remove account? The configurations saved will not be lost.'),
    ];

    $form['actions'] = ['#type' => 'actions'];
    $form['actions']['send'] = [
      '#type' => 'submit',
      '#value' => $this->t('Confirm'),
      '#attributes' => [
        'class' => [
          'use-ajax',
        ],
      ],
      '#ajax' => [
        'callback' => [$this, 'submitModalFormAjax'],
        'event' => 'click',
      ],
    ];

    $form['#attached']['library'][] = 'core/drupal.dialog.ajax';
    return $form;
  }

  /**
   *
   */
  public function submitModalFormAjax(array $form, FormStateInterface $form_state) {
    $editConfig = \Drupal::configFactory()->getEditable('user_provisioning.settings');
    $response   = new AjaxResponse();
    // If there are any form errors, AJAX replace the form.
    if ($form_state->hasAnyErrors()) {
      $response->addCommand(new ReplaceCommand('#modal_example_form', $form));
    }
    else {
      $editConfig->clear('mo_user_provisioning_customer_email')
        ->clear('mo_user_provisioning_customer_phone')
        ->clear('mo_user_provisioning_customer_id')
        ->clear('mo_user_provisioning_customer_api_key')
        ->clear('mo_user_provisioning_customer_token')
        ->set('mo_user_provisioning_status', 'CUSTOMER_SETUP')
        ->save();

      \Drupal::messenger()->addMessage(t('Your account has been removed successfully!'), 'status');
      $response->addCommand(new RedirectCommand(Url::fromRoute('user_provisioning.customer_setup')->toString()));
    }
    return $response;
  }

  /**
   *
   */
  public function validateForm(array &$form, FormStateInterface $form_state){}

  /**
   *
   */
  public function submitForm(array &$form, FormStateInterface $form_state){}

}
